# crypto-reporter

[![Build Status](https://ci.cuzo.dev/api/badges/parra/crypto-reporter/status.svg)](https://ci.cuzo.dev/parra/crypto-reporter)

Obtiene y publica precios de criptomonedas.

## Instalación y ejecución desde CLI

1. Descargar el repositorio en una nueva carpeta:

2. Copiar el fichero `env_file` a `.env` y abrirlo con un editor de textos y editarlo 
con la información del servidor MQTT, así como editar los tópicos que se quieran usar 
y el tiempo de refresco.

3. Dar permisos de ejecución al fichero principal con `chmod +x main.py` y ejecutar con `./main.py`.

## Instalación y ejecución desde Docker

1. Crear una carpeta (por ejemplo `crypto-reporter`) y acceder a ella.

2. Crear un fichero `.env` con el contenido del fichero `env_file` de este repositorio y 
editarlo con la información del servidor MQTT.

3. Crear un fichero `docker-compose.yml` como el siguiente:
````yml
version: '3'

services:
  crypto-reporter:
    image: parrazam/crypto-reporter
    container_name: crypto-reporter
    restart: unless-stopped
    volumes:
      - ./.env:/.env:ro
````

4. Ejecutar con el comando `docker compose up -d`.

## Menciones

La API se extrae de Coingecko y se puede consultar aquí: https://www.coingecko.com/en/api/documentation
